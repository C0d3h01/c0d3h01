<h1 align="center">
    <img src="https://readme-typing-svg.herokuapp.com/?font=Righteous&size=50&center=true&vCenter=true&width=1500&height=75&duration=2500&lines=Hi+There!+👋;+I'm+C0D3H01!" />
</h1>

<div align="center">
Hi, my name is Harshal Sawant. I am:

🇮🇳 He/Him from India

💻 A passionate guy from India with some knowledge of Android & Magisk Module Development

</div>

<div align="center"> 
  <a href="https://t.me/c0d3h01">
    <img src="https://img.shields.io/badge/Contact-333333?style=for-the-badge&logo=telegram&logoColor=blue" />
  </a>
</div>
 
<h2 align="center"> Experienced </h2>

<div align="center">
    <img src="https://skillicons.dev/icons?i=python,bash,github,vscode,linux,kotlin,androidstudio,nodejs" />
</div>
